package pbd.jacquest;

public class Chest
{
    private String id;
    private String bssid;
    private int wifi;
    private GeoPos chestPos;

    private double bearing;
    private double distance;

    public Chest(String id, String bssid, double distance, double degree, GeoPos currentPos)
    {
        this.id = id;
        this.bssid = bssid;
        this.chestPos = GeoPos.calculatePosisition(distance, degree, currentPos);
        this.wifi = 0;
        this.distance = distance;
        this.bearing = degree;
    }

    public void recalculateDistance(GeoPos pos)
    {
        distance = GeoPos.calculateDistance(pos, chestPos);
    }

    public void recalculateBearing(GeoPos pos)
    {
        bearing = GeoPos.calculateBearingHW(pos, chestPos);
    }

    public double getDistance()
    {
        return distance;
    }

    public double getBearing()
    {
        return bearing;
    }

    public String getId()
    {
        return id;
    }

    public String getBssid()
    {
        return bssid;
    }

    public int getWifi()
    {
        return wifi;
    }

    public GeoPos getChestPos()
    {
        return chestPos;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setBssid(String bssid)
    {
        this.bssid = bssid;
    }

    public void setWifi(int wifi)
    {
        this.wifi = wifi;
    }

    public void setChestPos(GeoPos geoPos)
    {
        this.chestPos = geoPos;
    }

    public String toString()
    {
        return "ID: " + id + " - " + chestPos.toString();
    }
}
