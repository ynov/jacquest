package pbd.jacquest;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class TrackView extends View
{
    public final float CENTER_X = 210;
    public final float CENTER_Y = 200;
    public final float RADIUS = 200;
    public final float DIAMETER = 2 * RADIUS;

    private float heading = 0;
    private float distance = 100;

    private Paint paint = new Paint();
    private Path path = new Path();

    public TrackView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public TrackView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public TrackView(Context context)
    {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.rotate(0 - heading, 210, 205);

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(2);
        paint.setStyle(Style.STROKE);
        canvas.drawCircle(CENTER_X, CENTER_Y, RADIUS, paint);

        paint.setStyle(Style.FILL_AND_STROKE);

        int red = 0 + (int) (distance * 2.55);
        int green = 255 - (int) (distance * 2.55);

        paint.setColor(Color.rgb(red > 255 ? 255 : red, green < 0 ? 0 : green, 0x00));

        path.reset();
        path.moveTo(-100 + CENTER_X, 0 + CENTER_Y);
        path.lineTo(0 + CENTER_X, -160 + CENTER_Y);
        path.lineTo(100 + CENTER_X, 0 + CENTER_Y);
        path.lineTo(40 + CENTER_X, -10 + CENTER_Y);
        path.lineTo(30 + CENTER_X, 160 + CENTER_Y);
        path.lineTo(-30 + CENTER_X, 160 + CENTER_Y);
        path.lineTo(-40 + CENTER_X, -10 + CENTER_Y);
        path.lineTo(-100 + CENTER_X, 0 + CENTER_Y);
        path.close();

        canvas.drawPath(path, paint);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(420, 420);
    }

    public float getHeading()
    {
        return heading;
    }

    public float getDistance()
    {
        return distance;
    }

    public void setHeading(float heading)
    {
        this.heading = heading;
    }

    public void setDistance(float distance)
    {
        this.distance = distance;
    }
}
