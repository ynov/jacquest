package pbd.jacquest;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class MainActivity extends Activity implements
    GooglePlayServicesClient.ConnectionCallbacks,
    GooglePlayServicesClient.OnConnectionFailedListener,
    LocationListener
{
    private Jacquest jq;

    private LocationRequest locationRequest;
    private LocationClient locationClient;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.text_info);

        jq = Jacquest.getInstance();

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setFastestInterval(1000);

        locationClient = new LocationClient(this, this, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    protected void onStart()
    {
        super.onStart();
        locationClient.connect();
    }

    @Override
    protected void onStop()
    {
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }

        locationClient.disconnect();
        super.onStop();
    }

    public void resetClickHandler(View view)
    {
        tv.setText("Wait...");
    }

    public void onLocationChanged(Location location)
    {
        jq.setLastPos(GeoPos.fromLocation(location));
        tv.setText("Position: " + location.getLatitude() + ", " + location.getLongitude());
    }

    public void onConnectionFailed(ConnectionResult result)
    {
        // ...
    }

    public void onConnected(Bundle connectionHint)
    {
        Toast.makeText(this, "Connected!", Toast.LENGTH_SHORT).show();
        locationClient.requestLocationUpdates(locationRequest, this);
    }

    public void onDisconnected()
    {
        Toast.makeText(this, "Disconnected!", Toast.LENGTH_SHORT).show();
    }
}
