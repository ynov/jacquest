package pbd.jacquest;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pbd.jacquest.api.DragonBallApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class RadarActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
        SensorEventListener
{
    public static final String MESSAGE0 = "RADAR_CHEST_LIST_INDEX";

    private DragonBallApi api;
    private Jacquest j = Jacquest.getInstance();

    private LocationRequest locationRequest;
    private LocationClient locationClient;

    private RadarView radarView;
    private TextView headingInfo;
    private TextView lastPosInfo;
    private TextView unachievedChestCountInfo;
    private TextView chestInRadarInfo;

    private SensorManager sensorManager;
    private Sensor magnetfield;
    private Sensor accelerometer;

    private Context context = this;

    private ProgressDialog refreshProgress = null;
    private ProgressDialog resetProgress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radar);

        /*
         * DragonBallApi.overrideVars(
         * "http://192.168.1.80/public/pbd/DragonBall/index.php",
         * "6ca8cf895147bf45707e65b0355b6cb3");
         */

        api = new DragonBallApi();

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationClient = new LocationClient(this, this, this);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        magnetfield = sensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometer = sensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        String message = getIntent().getStringExtra(CheckInActivity.MESSAGE0);
        if (message != null) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

        radarView = (RadarView) findViewById(R.id.radar_view);
        headingInfo = (TextView) findViewById(R.id.heading_info);
        lastPosInfo = (TextView) findViewById(R.id.last_position_info);
        unachievedChestCountInfo = (TextView) findViewById(R.id.unachieved_ball_count_info);
        chestInRadarInfo = (TextView) findViewById(R.id.chest_in_radar_info);

        setTitle(getString(R.string.title_activity_radar));
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this, magnetfield,
                SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause()
    {
        sensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        locationClient.connect();
    }

    @Override
    protected void onStop()
    {
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }

        locationClient.disconnect();

        super.onStop();
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.radar, menu);
        return true;
    }

    public void onLocationChanged(Location location)
    {
        j.setLastPos(GeoPos.fromLocation(location));
        lastPosInfo.setText("Your Position: " + j.getLastPos().toString());

        for (Chest chest : j.getChests()) {
            chest.recalculateBearing(j.getLastPos());
            chest.recalculateDistance(j.getLastPos());
        }

        radarView.invalidate();
    }

    public void onConnectionFailed(ConnectionResult result)
    {
    }

    public void onConnected(Bundle connectionHint)
    {
        Toast.makeText(this, "Connected to GPS!", Toast.LENGTH_SHORT).show();
        locationClient.requestLocationUpdates(locationRequest, this);

        j.setLastPos(GeoPos.fromLocation(locationClient.getLastLocation()));
        lastPosInfo.setText("Your Position: " + j.getLastPos().toString());

        if (checkConnection()) {
            refreshProgress = ProgressDialog.show(this, "Refreshing",
                    "Refreshing...", true);
            new RetrieveChestTask().execute(j.getLastPos());
            new ReceiveUnachievedChestCountTask().execute();
        }
    }

    public void onDisconnected()
    {
        Toast.makeText(this, "Disconnected to GPS!", Toast.LENGTH_SHORT).show();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    private float gravity[] = new float[3];
    private float geomagnetic[] = new float[3];

    public void onSensorChanged(SensorEvent event)
    {
        switch (event.sensor.getType()) {
        case Sensor.TYPE_ACCELEROMETER:
            System.arraycopy(event.values, 0, gravity, 0, 3);
            break;
        case Sensor.TYPE_MAGNETIC_FIELD:
            System.arraycopy(event.values, 0, geomagnetic, 0, 3);
            break;
        }

        if (gravity != null && geomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, gravity,
                    geomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);

                float heading = (float) GeoPos.radToDeg(orientation[0]);

                String headingInfoText = String.format(Locale.US,
                        "Device Heading: %.2f°", heading);
                headingInfo.setText(headingInfoText);

                radarView.setHeading(heading);
                radarView.invalidate();
            }
        }
    }

    private void dismissProgresses()
    {
        if (resetProgress != null) {
            resetProgress.dismiss();
            resetProgress = null;
        }

        if (refreshProgress != null) {
            refreshProgress.dismiss();
            refreshProgress = null;
        }
    }

    private boolean checkConnection()
    {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        } else {
            Toast.makeText(this, "Couldn't connect to the Internet",
                    Toast.LENGTH_SHORT).show();
            dismissProgresses();
            return false;
        }
    }

    // ---------------------------------------------------------------------------------------------
    // UI Event Handlers
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    public void refreshHandler(MenuItem menu)
    {
        if (checkConnection()) {
            refreshProgress = ProgressDialog.show(this, "Refreshing",
                    "Refreshing...", true);
            new RetrieveChestTask().execute(j.getLastPos());
            new ReceiveUnachievedChestCountTask().execute();
        }
    }

    public void resetHandler(MenuItem menu)
    {
        if (checkConnection()) {
            resetProgress = ProgressDialog.show(this, "Resetting",
                    "Resetting...", true);
            new ResetTask().execute();
            new RetrieveChestTask().execute(j.getLastPos());
            new ReceiveUnachievedChestCountTask().execute();
        }
    }

    public void trackClickHandler(View view)
    {
        final List<Chest> chests = j.getChests();

        if (!chests.isEmpty()) {
            int length = chests.size();
            String keys[] = new String[length];
            for (int i = 0; i < length; i++) {
                keys[i] = "Chest " + (i + 1);
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Track ...").setItems(keys,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which)
                        {
                            Intent intent = new Intent(context,
                                    TrackActivity.class);
                            intent.putExtra(MESSAGE0, which);
                            startActivity(intent);

                            // Toast.makeText(context,
                            // chests.get(which).getId(),
                            // Toast.LENGTH_SHORT).show();
                        }
                    });

            AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Toast.makeText(this, "There are no chests in range to track!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    // ---------------------------------------------------------------------------------------------
    // AsyncTasks
    // ---------------------------------------------------------------------------------------------
    // ---------------------------------------------------------------------------------------------
    private class RetrieveChestTask extends AsyncTask<GeoPos, Void, JSONObject>
    {
        @Override
        protected JSONObject doInBackground(GeoPos... params)
        {
            GeoPos pos = params[0];

            return api.retrieveChestLocation(String.valueOf(pos.getLatitude()),
                    String.valueOf(pos.getLongitude()));
        }

        @Override
        protected void onPostExecute(JSONObject result)
        {
            if (result != null) {
                try {
                    if (!result.isNull("data")) {
                        JSONArray chestData = result.getJSONArray("data");
                        j.setChestListFromJSON(chestData);
                        chestInRadarInfo.setText("# of chest(s) in radar: "
                                + chestData.length());
                    } else {
                        j.setChests(new ArrayList<Chest>());
                        chestInRadarInfo
                                .setText(getString(R.string.chest_in_radar_info_default));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "API Error", Toast.LENGTH_SHORT).show();
            }

            dismissProgresses();
            radarView.invalidate();
        }
    }

    private class ReceiveUnachievedChestCountTask extends
            AsyncTask<Void, Void, JSONObject>
    {
        @Override
        protected JSONObject doInBackground(Void... params)
        {
            return api.getUnachievedChestCount();
        }

        @Override
        protected void onPostExecute(JSONObject result)
        {
            if (result != null) {
                j.setUnachievedChestCountFromJSON(result);
                unachievedChestCountInfo.setText("# of unachieved chest(s): "
                        + j.getUnachievedChestCount());
            } else {
                Toast.makeText(context, "API Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class ResetTask extends AsyncTask<Void, Void, JSONObject>
    {
        @Override
        protected JSONObject doInBackground(Void... params)
        {
            return api.resetChest();
        }

        @Override
        protected void onPostExecute(JSONObject result)
        {
            if (result != null) {
                try {
                    if (result.getString("status").equals("success")) {
                        j.setChests(new ArrayList<Chest>());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                refreshProgress = ProgressDialog.show(context, "Refreshing",
                        "Refreshing...", true);
            } else {
                Toast.makeText(context, "API Error", Toast.LENGTH_SHORT).show();
            }

            dismissProgresses();
            radarView.invalidate();
        }
    }
}
