package pbd.jacquest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import pbd.jacquest.api.DragonBallApi;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class CheckInActivity extends Activity implements PictureCallback
{
    public static final String MESSAGE0 = "CHECKIN_SUCCESS_MESSAGE";

    private static final String FILENAME = "jacquest_capture.jpg";

    private Camera camera;
    private CameraView cameraView;

    private int chestListIndex;
    private Chest chest;

    private Context context = this;

    private ProgressDialog acquireProgress = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);

        chestListIndex = getIntent().getIntExtra(TrackActivity.MESSAGE0, 0);
        chest = Jacquest.getInstance().getChests().get(chestListIndex);

        camera = Camera.open();
        camera.setDisplayOrientation(90);

        cameraView = (CameraView) findViewById(R.id.camera_view);
        cameraView.initCamera(camera);

        setupActionBar();
    }

    @Override
    protected void onPause()
    {
        if (camera != null) {
            cameraView.setCamera(null);
            camera.release();
            camera = null;
        }
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if (camera == null) {
            camera = Camera.open();
            camera.setDisplayOrientation(180);
            cameraView.setCamera(camera);
        }
    }

    @Override
    protected void onStart()
    {
        super.onStart();
    }

    @Override
    protected void onStop()
    {
        super.onStop();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.check_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void checkinCaptureButtonHandler(View view)
    {
        camera.takePicture(null, null, this);
        acquireProgress = ProgressDialog.show(this, "Acquiring...",
                "Acquiring...", true);
        new AcquireTask().execute();
    }

    public void exifSetGeotag(File pictureFile)
    {
        GeoPos pos = Jacquest.getInstance().getLastPos();
        float[] latitude = GeoPos.degToDegMinSec((float) pos.getLatitude());
        float[] longitude = GeoPos.degToDegMinSec((float) pos.getLongitude());

        try {
            ExifInterface exif = new ExifInterface(pictureFile.getPath());

            String latitudeStr = (int) latitude[0] + "/1," + (int) latitude[1]
                    + "/1," + (int) (latitude[2] * 1000) + "/1000";
            String longitudeStr = (int) longitude[0] + "/1,"
                    + (int) longitude[1] + "/1," + (int) (longitude[2] * 1000)
                    + "/1000";

            exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE, latitudeStr);
            exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE, longitudeStr);

            Log.v("GEOPOS", latitudeStr);
            Log.v("GEOPOS", longitudeStr);

            if (latitude[3] == 2) {
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
            } else {
                exif.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
            }

            if (longitude[3] == 2) {
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
            } else {
                exif.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
            }

            exif.saveAttributes();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onPictureTaken(byte[] data, Camera camera)
    {
        File pictureFile = new File(Environment.getExternalStorageDirectory(),
                FILENAME);

        if (pictureFile.exists()) {
            pictureFile.delete();
        }

        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
            exifSetGeotag(pictureFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private class AcquireTask extends AsyncTask<Void, Void, JSONObject>
    {
        @Override
        protected JSONObject doInBackground(Void... params)
        {
            File pictureFile = new File(
                    Environment.getExternalStorageDirectory(), FILENAME);

            DragonBallApi api = new DragonBallApi();
            return api.acquireChest(chest.getId(), chest.getBssid(), "-40", pictureFile);
        }

        @Override
        protected void onPostExecute(JSONObject result)
        {
            if (result != null) {
                try {
                    if (result.getString("status").equals("success")) {
                        Intent intent = new Intent(context, RadarActivity.class);
                        intent.putExtra(MESSAGE0, "Acquire chest successful!");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    } else {
                        Toast.makeText(context, result.toString(), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(context, "API Error", Toast.LENGTH_SHORT).show();
            }

            if (acquireProgress != null) {
                acquireProgress.dismiss();
            }
        }
    }
}
