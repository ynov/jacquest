package pbd.jacquest;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pbd.jacquest.api.DragonBallApi;

public class Jacquest
{
    private static Jacquest instance = null;
    public static Jacquest getInstance()
    {
        if (instance == null) {
            instance = new Jacquest();
        }

        return instance;
    }

    private DragonBallApi api = new DragonBallApi();
    private GeoPos currentPos = null;
    private List<Chest> chests = new ArrayList<Chest>();
    private Chest trackedChest = null;
    private int unachievedChestCount = 0;

    private Jacquest()
    {
        // Jacquest!
    }

    public GeoPos getLastPos()
    {
        return currentPos;
    }

    public List<Chest> getChests()
    {
        return chests;
    }

    public Chest getTrackedChest()
    {
        return trackedChest;
    }

    public int getUnachievedChestCount()
    {
        return unachievedChestCount;
    }

    public DragonBallApi api() {
        return api;
    }

    // Setters
    public void setChestListFromJSON(JSONArray data)
    {
        chests = new ArrayList<Chest>();

        try {
            for (int i = 0; i < data.length(); i++) {
                JSONObject obj  = data.getJSONObject(i);
                String id       = obj.getString("id");
                String bssid    = obj.getString("bssid");
                double distance = obj.getDouble("distance");
                double degree   = obj.getDouble("degree");

                chests.add(new Chest(id, bssid, distance, degree, currentPos));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setUnachievedChestCountFromJSON(JSONObject json)
    {
        try {
            unachievedChestCount = json.getInt("data");
        } catch (JSONException e) {
            unachievedChestCount = -1;
            e.printStackTrace();
        }
    }

    public void setLastPos(GeoPos pos)
    {
        this.currentPos = pos;
    }

    public void setChests(List<Chest> chests)
    {
        this.chests = chests;
    }

    public void setTrackedChest(Chest chest)
    {
        this.trackedChest = chest;
    }

    public void setUnachievedChestCount(int unachievedChestCount)
    {
        this.unachievedChestCount = unachievedChestCount;
    }
}
