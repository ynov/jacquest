package pbd.jacquest;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class CameraView extends SurfaceView implements SurfaceHolder.Callback
{
    private SurfaceHolder holder;
    private Camera camera = null;

    public CameraView(Context context, Camera camera)
    {
        super(context);
        initCamera(camera);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public CameraView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public CameraView(Context context)
    {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(460, 345);
    }

    public void setCamera(Camera camera)
    {
        this.camera = camera;
    }

    public void initCamera(Camera camera)
    {
        this.camera = camera;

        holder = getHolder();
        holder.addCallback(this);

        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        try {
            if (camera != null) {
                camera.setPreviewDisplay(holder);
                Parameters cp = camera.getParameters();
                cp.setPictureSize(640, 480);

                camera.setParameters(cp);
                camera.startPreview();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width,
            int height)
    {
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {
        if (holder.getSurface() == null) {
            return;
        }

        try {
            camera.stopPreview();
        } catch (Exception e) {
        }

        try {
            if (camera != null) {
                camera.setPreviewDisplay(holder);

                camera.setPreviewDisplay(holder);
                Parameters cp = camera.getParameters();
                cp.setPictureSize(640, 480);

                camera.startPreview();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
