package pbd.jacquest;

import static pbd.jacquest.GeoPos.degToRad;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

public class RadarView extends View
{
    public final float CENTER_X = 210;
    public final float CENTER_Y = 200;
    public final float RADIUS   = 200;
    public final float DIAMETER = 2 * RADIUS;

    public float heading = 0;

    private Paint paint = new Paint();

    public RadarView(Context context)
    {
        super(context);
    }

    public RadarView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    public RadarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public void setHeading(float heading)
    {
        this.heading = heading;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        setMeasuredDimension(420, 420);
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        canvas.rotate(0 - heading, 210, 205);

        paint.setColor(Color.BLACK);
        paint.setStrokeWidth(2);
        paint.setStyle(Style.STROKE);
        canvas.drawCircle(CENTER_X, CENTER_Y, RADIUS, paint);

        paint.setColor(Color.DKGRAY);
        paint.setStyle(Style.FILL_AND_STROKE);
        canvas.drawCircle(CENTER_X, CENTER_Y, RADIUS - 2, paint);

        paint.setColor(Color.GRAY);
        paint.setStyle(Style.STROKE);
        canvas.drawLine(CENTER_X, 0, CENTER_X, DIAMETER, paint);
        canvas.drawLine(10, CENTER_Y, DIAMETER + 10, CENTER_Y, paint);

        paint.setColor(Color.RED);
        paint.setStyle(Style.FILL_AND_STROKE);
        canvas.drawText("NORTH", 190, 25, paint);
        canvas.drawCircle(CENTER_X, CENTER_Y, 8, paint);

        // Draw chests
        int i = 1;
        List<Chest> chests = Jacquest.getInstance().getChests();
        for (Chest chest : chests) {
            drawChest(canvas, i, chest.getDistance(), chest.getBearing());
            i++;
        }
    }

    public void drawChest(Canvas canvas, int id, double distance, double angleDegree)
    {
        double angle = degToRad(angleDegree);

        double x = -1 * distance * Math.cos(angle + Math.PI / 2);
        double y = distance * Math.sin(angle - Math.PI / 2);

        double xx = x * 2 + CENTER_X;
        double yy = y * 2 + CENTER_Y;

        paint.setStyle(Style.FILL_AND_STROKE);
        paint.setColor(Color.BLACK);
        canvas.drawCircle((float) xx, (float) yy, 10, paint);
        paint.setColor(Color.YELLOW);
        canvas.drawCircle((float) xx, (float) yy, 8, paint);
        paint.setColor(Color.GREEN);
        canvas.drawText("" + id, (float) xx - 15, (float) yy - 10, paint);
    }
}
