package pbd.jacquest;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

public class TrackActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener, LocationListener,
        SensorEventListener
{
    public static final String MESSAGE0 = "TRACK_CHEST_LIST_INDEX";

    private Jacquest j = Jacquest.getInstance();

    private LocationRequest locationRequest;
    private LocationClient locationClient;

    private SensorManager sensorManager;
    private Sensor magnetfield;
    private Sensor accelerometer;

    private TrackView trackView;
    private TextView trackedChestIdInfo;
    private TextView trackedChestBssidInfo;

    // private Context context = this;

    private int chestListIndex;
    private Chest trackedChest = null;
    private float heading = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);

        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        locationClient = new LocationClient(this, this, this);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        magnetfield = sensorManager
                .getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometer = sensorManager
                .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        chestListIndex = getIntent().getIntExtra(RadarActivity.MESSAGE0, 0);
        trackedChest = j.getChests().get(chestListIndex);

        trackView = (TrackView) findViewById(R.id.track_view);
        trackedChestIdInfo = (TextView) findViewById(R.id.tracked_chest_id_info);
        trackedChestBssidInfo = (TextView) findViewById(R.id.tracked_chest_bssid_info);

        trackedChestIdInfo.setText("Chest ID: " + trackedChest.getId());
        trackedChestBssidInfo.setText("BSSID: " + trackedChest.getBssid());

        setupActionBar();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void setupActionBar()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    public void checkinButtonClickHandler(View view)
    {
        Intent intent = new Intent(this, CheckInActivity.class);
        intent.putExtra(MESSAGE0, chestListIndex);
        startActivity(intent);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        sensorManager.registerListener(this, magnetfield,
                SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, accelerometer,
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause()
    {
        sensorManager.unregisterListener(this);
        super.onPause();
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        locationClient.connect();
    }

    @Override
    protected void onStop()
    {
        if (locationClient.isConnected()) {
            locationClient.removeLocationUpdates(this);
        }

        locationClient.disconnect();

        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.track, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onLocationChanged(Location location)
    {
        j.setLastPos(GeoPos.fromLocation(location));
        trackedChest.recalculateBearing(j.getLastPos());
        trackedChest.recalculateDistance(j.getLastPos());
        trackView.setDistance((float) trackedChest.getDistance());
        trackView.setHeading((float) (heading - trackedChest.getBearing()));
        trackView.invalidate();
    }

    public void onConnectionFailed(ConnectionResult result)
    {
    }

    public void onConnected(Bundle connectionHint)
    {
        Toast.makeText(this, "Connected to GPS!", Toast.LENGTH_SHORT).show();
        locationClient.requestLocationUpdates(locationRequest, this);

        j.setLastPos(GeoPos.fromLocation(locationClient.getLastLocation()));
        trackedChest.recalculateBearing(j.getLastPos());
        trackedChest.recalculateDistance(j.getLastPos());
        trackView.setDistance((float) trackedChest.getDistance());
    }

    public void onDisconnected()
    {
        Toast.makeText(this, "Disconnected to GPS!", Toast.LENGTH_SHORT).show();
    }

    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
    }

    private float gravity[] = new float[3];
    private float geomagnetic[] = new float[3];

    public void onSensorChanged(SensorEvent event)
    {
        switch (event.sensor.getType()) {
        case Sensor.TYPE_ACCELEROMETER:
            System.arraycopy(event.values, 0, gravity, 0, 3);
            break;
        case Sensor.TYPE_MAGNETIC_FIELD:
            System.arraycopy(event.values, 0, geomagnetic, 0, 3);
            break;
        }

        if (gravity != null && geomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, gravity,
                    geomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);

                heading = (float) GeoPos.radToDeg(orientation[0]);
                trackView.setHeading((float) (heading - trackedChest
                        .getBearing()));
                trackView.invalidate();
            }
        }
    }
}
