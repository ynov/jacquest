package pbd.jacquest.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class JacquestHttpClient
{
    public static final String ENCODING = "utf-8";

    public JacquestHttpClient()
    {
        // JacquestHttpClient!
    }

    private String readHttpEntityToString(HttpEntity entity)
    {
        StringBuilder responseText = new StringBuilder();
        try {
            if (entity != null) {
                InputStream in = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    responseText.append(line);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseText.toString();
    }

    public String doHttpRequest(String method, String url, Map<String, String> queryParams, Map<String, File> binParams)
    {
        HttpClient httpClient = new DefaultHttpClient();
        HttpResponse response = null;
        HttpEntity entity = null;

        String _url = url;
        String responseText = "";

        if (method.equals("GET")) {
            if (queryParams != null) {
                _url += "?" + URLEncodedUtils.format(mapToNVPList(queryParams), ENCODING);
            }
            HttpGet httpGet = new HttpGet(_url);

            try {
                response = httpClient.execute(httpGet);
                entity = response.getEntity();

                responseText = readHttpEntityToString(entity);
            }
            catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        } else if (method.equals("POST")) {
            HttpPost httpPost = new HttpPost(_url);
            if (queryParams != null) {
                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(mapToNVPList(queryParams), ENCODING));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }

            try {
                response = httpClient.execute(httpPost);
                entity = response.getEntity();

                responseText = readHttpEntityToString(entity);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        } else if (method.equals("POST|multipart")) {
            HttpPost httpPost = new HttpPost(_url);

            MultipartEntity reqEntity = new MultipartEntity();
            if (queryParams != null) {
                for (String key : queryParams.keySet()) {
                    try {
                        reqEntity.addPart(key, new StringBody(queryParams.get(key)));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (binParams != null) {
                for (String key : binParams.keySet()) {
                    reqEntity.addPart(key, new FileBody(binParams.get(key)));
                }
            }

            httpPost.setEntity(reqEntity);
            try {
                response = httpClient.execute(httpPost);
                entity = response.getEntity();

                responseText = readHttpEntityToString(entity);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        return responseText;
    }

    public String doHttpGet(String url, Map<String, String> queryParams)
    {
        return doHttpRequest("GET", url, queryParams, null);
    }

    public String doHttpPost(String url, Map<String, String> queryParams)
    {
        return doHttpRequest("POST", url, queryParams, null);
    }

    public String doHttpPostMultipart(String url, Map<String, String> queryParams, Map<String, File> binParams)
    {
        return doHttpRequest("POST|multipart", url, queryParams, binParams);
    }

    public List<NameValuePair> mapToNVPList(Map<String, String> map)
    {
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        for (String key : map.keySet()) {
            nvps.add(new BasicNameValuePair(key, map.get(key)));
        }

        return nvps;
    }
}
