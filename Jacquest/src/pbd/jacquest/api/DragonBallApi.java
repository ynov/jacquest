package pbd.jacquest.api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class DragonBallApi
{
    /* Constants */
    public static String URL      = "http://milestone.if.itb.ac.id/pbd/index.php";
    public static String GROUPID  = "6ca8cf895147bf45707e65b0355b6cb3";

    public static void overrideVars(String url, String groupId)
    {
        DragonBallApi.URL     = url;
        DragonBallApi.GROUPID = groupId;
    }

    public DragonBallApi()
    {
        // DragonBallApi!
    }

    private Map<String, String> createDefaultParams(String action)
    {
        Map<String, String> params = new HashMap<String, String>();
        params.put("group_id", GROUPID);
        if (action != null) {
            params.put("action", action);
        }

        return params;
    }

    /**
     * (1)
     * Reset all chests status to "not acquired" (zero)
     *
     * Expected return:
     * <pre>
     * { "status": "success" }
     * </pre>
     *
     * Possible return:
     * <pre>
     * { "status": "failed", "description": error_message" }
     * </pre>
     *
     * @return JSONObject as in expected/possible return
     */
    public JSONObject resetChest()
    {
        Map<String, String> params = createDefaultParams("reset");
        JacquestHttpClient jhc = new JacquestHttpClient();
        String res = jhc.doHttpPost(URL, params);

        if (res == null) {
            return null;
        }

        JSONObject json = null;
        try {
            json = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    /**
     * (2)
     * Returns unachieved chests in radius of 100m from archeolog's position (latitude + longitude)
     *
     * Expected return:
     * <pre>
     * {
     *   "status": "success",
     *   "data": [
     *     {
     *       "id": chest_id,
     *       "distance": distance_in_meter,
     *       "degree": degree
     *     }, ...
     *   ]
     * }
     * </pre>
     *
     * Possible return:
     * <pre>{ "status": "failed", "description": error_message" }</pre>
     *
     * @param latitude String
     * @param longitude String
     * @return JSONObject as in expected/possible return
     */
    public JSONObject retrieveChestLocation(String latitude, String longitude)
    {
        Map<String, String> params = createDefaultParams("retrieve");
        params.put("latitude", latitude);
        params.put("longitude", longitude);

        JacquestHttpClient jhc = new JacquestHttpClient();
        String res = jhc.doHttpGet(URL, params);

        if (res == null) {
            return null;
        }

        JSONObject json = null;
        try {
            json = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    /**
     * (3)
     * Returns the count of unachieved chests
     *
     * Expected return:
     * <pre>
     * { "status": "success", data: chest_count }
     * </pre>
     *
     * Possible return:
     * <pre>
     * { "status": "failed", "description": error_message" }
     * </pre>
     *
     * @return JSONObject as in expected/possible return
     */
    public JSONObject getUnachievedChestCount()
    {
        Map<String, String> params = createDefaultParams("number");

        JacquestHttpClient jhc = new JacquestHttpClient();
        String res = jhc.doHttpGet(URL, params);

        if (res == null) {
            return null;
        }

        JSONObject json = null;
        try {
            json = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }

    /**
     * (4)
     *
     * Expected return:
     * <pre>
     * { "status": "success" }
     * </pre>
     *
     * Possible return:
     * <pre>
     * { "status": "failed", "description": error_message" }
     * </pre>
     *
     * @return JSONObject as in expected/possible return
     */
    public JSONObject acquireChest(String chestId, String bssid, String wifi, File file)
    {
        Map<String, String> params = createDefaultParams("acquire");
        params.put("chest_id", chestId);
        params.put("bssid", bssid);
        params.put("wifi", wifi);

        Map<String, File> binParams = new HashMap<String, File>();
        binParams.put("file", file);

        JacquestHttpClient jhc = new JacquestHttpClient();
        String res = jhc.doHttpPostMultipart(URL, params, binParams);

        if (res == null) {
            return null;
        }

        JSONObject json = null;
        try {
            json = new JSONObject(res);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return json;
    }
}
