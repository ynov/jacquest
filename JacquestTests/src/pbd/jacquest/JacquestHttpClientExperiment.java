package pbd.jacquest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import pbd.jacquest.api.DragonBallApi;
import pbd.jacquest.api.JacquestHttpClient;

public class JacquestHttpClientExperiment
{
    public static void doHttpConnectionExperiment()
    {
        HttpClient client = new DefaultHttpClient();

        JacquestHttpClient jhc = new JacquestHttpClient();

        Map<String, String> kv = new HashMap<String, String>();
        kv.put("q", "hello");
        kv.put("nasi", "goreng");

        StringBuilder sb = null;
        try {
            HttpGet httpGet = new HttpGet("http://www.google.com/?" + URLEncodedUtils.format(jhc.mapToNVPList(kv), "utf-8"));

            System.out.println(httpGet.getURI());
            HttpResponse response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream in = entity.getContent();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(in));
                String line = null;
                sb = new StringBuilder();
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                    sb.append("\n");
                }

                in.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(sb.toString());
    }

    public static void doJacquestHttpClientExperiment()
    {
        JacquestHttpClient httpClient = new JacquestHttpClient();

        Map<String, String> params1 = new HashMap<String, String>();
        params1.put("action", "retrieve");
        params1.put("group_id", DragonBallApi.GROUPID);
        params1.put("latitude", "-6.890608");
        params1.put("longitude", "107.610008");

        String response = httpClient.doHttpGet(DragonBallApi.URL, params1);

        System.out.println(response);
    }

    public static void main(String args[])
    {
        doHttpConnectionExperiment();
        doJacquestHttpClientExperiment();
    }
}
