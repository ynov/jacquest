package pbd.jacquest.test;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

import pbd.jacquest.api.DragonBallApi;

public class Experiment
{
    @Test
    public void dragonBallApiExperiment()
    {
        DragonBallApi api = new DragonBallApi();

        System.out.println(api.resetChest().toString());
        System.out.println(api.retrieveChestLocation("-6.890608", "107.610008").toString());
        System.out.println(api.getUnachievedChestCount().toString());

        File file = new File("/tmp/dummy.jpg");
        String acquire = api.acquireChest("374f4bdbc76bda84235baeb80c86df1e", "68:7f:74:70:11:1c", "100", file).toString();
        System.out.println(acquire);

        assertTrue(true);
    }

}
