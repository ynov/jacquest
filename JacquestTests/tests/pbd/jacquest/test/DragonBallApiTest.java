package pbd.jacquest.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import pbd.jacquest.Chest;
import pbd.jacquest.GeoPos;
import pbd.jacquest.Jacquest;
import pbd.jacquest.api.DragonBallApi;

public class DragonBallApiTest
{
    @BeforeClass
    public static void setupBeforeClass()
    {
        DragonBallApi.overrideVars("http://localhost/public/pbd/DragonBall/index.php", "6ca8cf895147bf45707e65b0355b6cb3");
    }

    @Test
    public void resetChestTest()
    {
        DragonBallApi api = new DragonBallApi();
        JSONObject res = api.resetChest();

        assertNotNull(res);
        try {
            assertEquals("success", res.getString("status"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("Reset Test");
        System.out.println(res.toString());
    }

    @Test
    public void retrieveChestLocation()
    {
        DragonBallApi api = new DragonBallApi();

        GeoPos pos = new GeoPos(-6.922601, 107.661096);
        Jacquest.getInstance().setLastPos(pos);

        String latitude = String.valueOf(pos.getLatitude());
        String longitude = String.valueOf(pos.getLongitude());

        JSONObject res = api.retrieveChestLocation(latitude, longitude);

        assertNotNull(res);
        try {
            assertEquals("success", res.getString("status"));

            if (!res.isNull("data")) {
                JSONArray chestData = res.getJSONArray("data");
                Jacquest.getInstance().setChestListFromJSON(chestData);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("Retrieve Test");
        System.out.println(res.toString());
        for (Chest chest : Jacquest.getInstance().getChests()) {
            System.out.println(chest.toString());
        }
    }

    @Test
    public void getUnachievedChestCountTest()
    {
        DragonBallApi api = new DragonBallApi();
        JSONObject res = api.getUnachievedChestCount();

        assertNotNull(res);
        try {
            assertNotNull(res.getInt("data"));
            assertTrue(res.getInt("data") >= 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("Data (Get Unachieved Chest) Test");
        System.out.println(res.toString());
    }

    @Test
    public void acquireChestTest()
    {
        DragonBallApi api = new DragonBallApi();
        String chestId = "6d73458e7cfb8269e0b91ed656451703";
        String bssid = "64:70:02:e3:db:bc";
        String wifi = String.format("%d", -88);
        File file = new File("/tmp/dummy.jpg");

        JSONObject res = api.acquireChest(chestId, bssid, wifi, file);

        assertNotNull(res);
        System.out.println("Acquire Test");
        System.out.println(res.toString());
    }
}
