package pbd.jacquest.test;

import static org.junit.Assert.*;

import org.junit.Test;

import pbd.jacquest.GeoPos;

public class GeoPosTest
{
    @Test
    public void testCalculatePostition()
    {
        System.out.println("Calculate position test, expected ~ -6.92242, 107.66065");
        GeoPos b = new GeoPos(-6.922601, 107.661096);

        GeoPos c = GeoPos.calculatePosisition(53.246025548098, 292, b);
        System.out.println(c.getLatitude() + ", " + c.getLongitude());
        assertTrue(true);
    }

    @Test
    public void testCalculateDistance()
    {
        System.out.println("Calculate distance test, expected ~ 53.00");
        GeoPos a = new GeoPos(-6.92242, 107.66065);
        GeoPos b = new GeoPos(-6.922601, 107.661096);

        //GeoPos a = new GeoPos(-6.9223355, 107.6609219);
        //GeoPos b = new GeoPos(-6.9225913, 107.6610341);

        double distance = GeoPos.calculateDistanceHW(a, b);
        System.out.println("Distance: " + distance);
        assertTrue(distance - 53.00 < 1.00);
    }

    @Test
    public void testCalculateBearing()
    {
        System.out.println("Calculate bearing, expected ~ 292");
        GeoPos target = new GeoPos(-6.92242, 107.66065);
        GeoPos start = new GeoPos(-6.922601, 107.661096);

        double bearing = GeoPos.calculateBearingHW(start, target);
        System.out.println("Bearing: " + bearing);
    }

    @Test
    public void testDegToDegMinSec()
    {
        System.out.println("Test Deg to DegMinSec");
        float deg = -6.92242f;
        float[] degMinSec = GeoPos.degToDegMinSec(deg);

        System.out.println("" + degMinSec[0] + "° " + degMinSec[1] + "′ " + degMinSec[2] + "″");
    }
}
